package util

import com.example.demo.entity.*
import com.example.demo.entity.dto.*
import com.example.demo.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    @Mappings(
            Mapping(source="manufacturer",target="manu")
    )
    fun mapProductDto(product: Product?):ProductDto?
    fun mapProductDto(products:List<Product>?):List<ProductDto>?
    @InheritInverseConfiguration
    fun mapProductDto(product: ProductDto):Product
    fun mapManufacturer(manu:Manufacturer):ManufacturerDto
    @InheritInverseConfiguration
    fun mapManufacturer(manu:ManufacturerDto):Manufacturer
    @Mappings(
            Mapping(source="customer.jwtUser.username",target = "username"),
            Mapping(source="customer.jwtUser.authorities",target = "authorities")
    )
    fun mapCustomerDto(customer: Customer?):CustomerDto?

    fun mapAuthority(authority: Authority):AuthorityDto
    fun mapAuthority(authority: List<Authority>):List<AuthorityDto>
    @InheritInverseConfiguration
    fun mapCustomerDto(customer: CustomerDto):Customer

    fun mapCustomerDto(customer: List<Customer>?):List<CustomerDto>?
    fun mapShoppingCartDto(shoppingcart: List<ShoppingCart>):List<ShoppingCartDto>

    fun mapShoppingCartDto(shoppingcart: ShoppingCart):ShoppingCartDto

    fun mapSelectedProductDto(selectPro:SelectedProduct):SelectedProductDto
    fun mapSelectedProductDto(selectPro:List<SelectedProduct>?):List<SelectedProductDto>?
}
