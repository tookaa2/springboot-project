package com.example.demo.controller

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import com.example.demo.entity.dto.CustomerDto
import com.example.demo.service.CustomerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import util.MapperUtil

@RestController
class CustomerController{
    @Autowired
    lateinit var customerService: CustomerService
    @GetMapping("/customer")
    fun getAllCustomers():ResponseEntity<Any>{
        val customers=customerService.getCustomers();
        var output=MapperUtil.INSTANCE.mapCustomerDto(customers)
        output?.let{return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/query")
    fun getCustomerByName(@RequestParam("name") name:String):ResponseEntity<Any>{
        var output=MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let{return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customer/queryPartial")
    fun getProductByPartialName(@RequestParam("name") name:String):ResponseEntity<Any>{
        var output=MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialName(name))
        output?.let{return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customer/queryPartialEmail")
    fun getProductByPartialEmail(@RequestParam("email") email:String):ResponseEntity<Any>{
        var output=MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialEmail(email))
        output?.let{return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/queryPartialProvince")
    fun getProductByPartialProvince(@RequestParam("province") province:String):ResponseEntity<Any>{
        var output=MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByProvince(province))
        output?.let{return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customer/status")
    fun getProductByStatus(@RequestParam("status") status:String):ResponseEntity<Any>{
        var output:List<Customer>?=null
        println(status)
        if(status=="PENDING"){
            output  = customerService.getCustomerByStatus(UserStatus.PENDING)
        }
        else if(status=="NOTACTIVE"){
            output  = customerService.getCustomerByStatus(UserStatus.NOTACTIVE)
        }
        else if(status=="DELETED"){
            output  = customerService.getCustomerByStatus(UserStatus.DELETED)
        }else{
            output  = customerService.getCustomerByStatus(UserStatus.ACTIVE)
        }

//        when(status){
//        "PENDING"-> output = customerService.getCustomerByStatus(UserStatus.PENDING)
//        "ACTIVE"->  output  = customerService.getCustomerByStatus(UserStatus.ACTIVE)
////        "NOTACTIVE"->output=MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByStatus(UserStatus.NOTACTIVE))
////        "DELETED"->output=MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByStatus(UserStatus.DELETED))
//        }
        output?.let{return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto)
            :ResponseEntity<Any>{
        customerDto.billingAddress=customerDto.defaultAddress
        val output:Customer=customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto:CustomerDto?=MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/address/{addrId}")
    fun addProductByManu(@RequestBody customerDto:CustomerDto,@PathVariable addrId:Long)
            :ResponseEntity<Any>{

        val output:Customer=customerService.save(addrId,MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto:CustomerDto?=MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @DeleteMapping("/customer/{cusId}")
    fun deleteCustomer(@PathVariable("cusId") id:Long)
    :ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val output=MapperUtil.INSTANCE.mapCustomerDto(customer)
        output?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }



}