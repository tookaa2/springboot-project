package com.example.demo.controller

import com.example.demo.entity.Customer
import com.example.demo.entity.dto.*
import com.example.demo.service.ShoppingCartService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import util.MapperUtil


@RestController
class ShoppingCartController{
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService
    @GetMapping("/shoppingcart")
    fun getAllShoppingCart():ResponseEntity<Any>{
        var shoppingcarts= shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingcarts))
    }

    @GetMapping("/shoppingcart/all")
    fun getProductWithPage(
            @RequestParam("page") page:Int,
            @RequestParam("pageSize") pageSize:Int
    ):ResponseEntity<Any>{
        val output= shoppingCartService.getShoppingCartWithPage(page,pageSize)
//        return ResponseEntity.ok(output)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingcarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content))
        )
    }

    @GetMapping("/shoppingcart/partialName")
    fun getProductWithPage(
            @RequestParam("name") name:String,
            @RequestParam("page") page:Int,
            @RequestParam("pageSize") pageSize:Int
    ):ResponseEntity<Any>{
        val output= shoppingCartService.getShoppingCartPartialNameWithPage(name,page,pageSize)
//        return ResponseEntity.ok(output)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingcarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content))
        )
    }

    @GetMapping("/customer/partialProductName")
    fun getCustomerWithPage(
            @RequestParam("name") name:String,
            @RequestParam("page") page:Int,
            @RequestParam("pageSize") pageSize:Int
    ):ResponseEntity<Any>{
        val output= shoppingCartService.getShoppingCartPartialNameWithPage(name,page,pageSize)
        val customerList= mutableListOf<Customer>()
        for (data in output.content){
            customerList.add(data.customer)
            }
//        return ResponseEntity.ok(output)
        return ResponseEntity.ok(CustomerPageDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                customers = MapperUtil.INSTANCE.mapCustomerDto(customerList))
        )
    }

    @PostMapping("/shoppingcart/{customerId}")
    fun addShoppingCartWithCustomerId(
            @PathVariable("customerId") id:Long?,@RequestBody shoppingCartDto: ShoppingCartDto
    ):ResponseEntity<Any>{

        if(id==null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id must not be null")
//        return ResponseEntity.ok()
        var output=MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCartService.save(id,shoppingCartDto))
                output?.let{
                   val outputProductlist= mutableListOf<MinifiedProductDto>()

                    for(product in output.selectedProduct!!){
                        outputProductlist.add(
                                MinifiedProductDto(
                                        name = product.product?.name,
                                        description = product.product?.description,
                                        quantity = product.quantity
                                )
                        )
                    }
                    return ResponseEntity.ok(ShoppingResultDto(
                            customer = output.customer?.name,
                            address = output.customer?.defaultAddress,
                            products = outputProductlist

                    )) }


    }


}