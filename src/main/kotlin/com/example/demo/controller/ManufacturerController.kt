package com.example.demo.controller

import com.example.demo.entity.dto.ManufacturerDto
import com.example.demo.service.ManufacturerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import util.MapperUtil
import javax.xml.ws.Response

@RestController
class ManufacturerController{
    @Autowired
    lateinit var manufacturerService: ManufacturerService
    @GetMapping("/manufacturer")
    fun getAllManufacturer():ResponseEntity<Any>{
        return ResponseEntity.ok(manufacturerService.getManufacturers());
    }
    @PostMapping("/manufacturer")
    fun addManufacturer(@RequestBody manu:ManufacturerDto) :ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }
    @PutMapping("/manufacturer")
    fun updateManufacturer(@RequestBody manu: ManufacturerDto)
    :ResponseEntity<Any>{
        if(manu.id==null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id must not be null")
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }
    @PutMapping("/manufacturer/{manuId}")
    fun updateManufacturer(@PathVariable("manuId")id:Long?,@RequestBody manu: ManufacturerDto)
            :ResponseEntity<Any>{
        manu.id=id
        if(manu.id==null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id must not be null")
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }
}