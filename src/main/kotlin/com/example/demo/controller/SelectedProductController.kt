package com.example.demo.controller

import com.example.demo.entity.dto.PageSelectedProductDto
import com.example.demo.service.SelectedProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import util.MapperUtil

@RestController
class SelectedProductController{
    @Autowired
    lateinit var selectedProductService: SelectedProductService
    @GetMapping("/selectedproduct/name")
    fun getProductWithPage(
            @RequestParam("name") name:String,
            @RequestParam("page") page:Int,
            @RequestParam("pageSize") pageSize:Int
    ): ResponseEntity<Any> {
        val output= selectedProductService.getSelectedProductByName(name,page,pageSize)
//        return ResponseEntity.ok(output)



        return ResponseEntity.ok(PageSelectedProductDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapSelectedProductDto(output.content))
        )
    }
}