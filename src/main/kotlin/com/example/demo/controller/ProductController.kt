package com.example.demo.controller

import com.example.demo.dao.ProductDao
import com.example.demo.entity.Product
import com.example.demo.entity.dto.PageProductDto
import com.example.demo.entity.dto.ProductDto
import com.example.demo.service.ProductService
import org.aspectj.lang.annotation.DeclareError
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import util.MapperUtil
import kotlin.reflect.jvm.internal.impl.builtins.jvm.MappingUtilKt

@RestController
class ProductController{
    @Autowired
    lateinit var productService: ProductService
        @GetMapping("/product")
        fun getAllProducts():ResponseEntity<Any>{
            val products = productService.getProducts()
//            val productDtos = mutableListOf<ProductDto>()
//            for (product in products){
//                productDtos.add(ProductDto(
//                        product.name,
//                        product.description,
//                        product.price,
//                        product.amountInStock,
//                        product.imageUrl
//                ))
//            }
            var output=MapperUtil.INSTANCE.mapProductDto(products)
            output?.let{return ResponseEntity.ok(it) }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        }

        @GetMapping("/product/query")
        fun getProductByName(@RequestParam("name") name:String):ResponseEntity<Any>{
            var output=MapperUtil.INSTANCE.mapProductDto(productService.getProductByName(name))
                    output?.let{return ResponseEntity.ok(it) }
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        }

        @GetMapping("/product/queryPartial")
        fun getProductByPartialName(@RequestParam("name") name:String):ResponseEntity<Any>{
            var output=MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
            output?.let{return ResponseEntity.ok(it) }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        }
        @GetMapping("/product/name")
        fun getProductWithPage(
                @RequestParam("name") name:String,
                @RequestParam("page") page:Int,
                @RequestParam("pageSize") pageSize:Int
        ):ResponseEntity<Any>{
            val output= productService.getProductWithPage(name,page,pageSize)
            return ResponseEntity.ok(PageProductDto(totalPages = output.totalPages,
                    totalElements = output.totalElements,
                    products = MapperUtil.INSTANCE.mapProductDto(output.content))
                    )
        }

    @PostMapping("/product")
    fun addProduct(@RequestBody productDto:ProductDto)
    :ResponseEntity<Any>{
        val output:Product=productService.save(MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto:ProductDto?=MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }


    @PostMapping("/product/manufacturer/{manuId}")
    fun addProductByManu(@RequestBody productDto: ProductDto,@PathVariable manuId:Long)
    :ResponseEntity<Any>{
        val output=productService.save(manuId,MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto=MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/product/{id}")
    fun deleteProduct(@PathVariable("id") id:Long):ResponseEntity<Any>{
        val product = productService.remove(id)
        val output=MapperUtil.INSTANCE.mapProductDto(product)
        output?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    }
