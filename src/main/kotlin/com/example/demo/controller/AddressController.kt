package com.example.demo.controller

import com.example.demo.entity.Address
import com.example.demo.service.AddressService

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import util.MapperUtil

@RestController
class AddressController{
    @Autowired
    lateinit var addressService: AddressService
    @PostMapping("/address")
    fun saveAddress(@RequestBody addr: Address) : ResponseEntity<Any> {
        return ResponseEntity.ok(addressService.save(addr))
    }
    @PutMapping("/address/{addID}")
    fun updateAddress(@PathVariable("addID")id:Long?, @RequestBody addr: Address)
            :ResponseEntity<Any>{
        addr.id=id
        if(addr.id==null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id must not be null")
        return ResponseEntity.ok(addressService.save(addr))
    }
}