package com.example.demo.repository

import com.example.demo.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository


interface ShoppingCartRepository: CrudRepository<ShoppingCart,Long>{
   fun findAll(page: Pageable): Page<ShoppingCart>
   fun findBySelectedProduct_Product_NameContainingIgnoreCase(name:String,page:Pageable):Page<ShoppingCart>
}