package com.example.demo.repository

import com.example.demo.entity.Product
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ProductRepository: CrudRepository<Product,Long>{

    fun findByName(name:String):Product?
    fun findByNameContainingIgnoreCase(name: String):List<Product>
    fun findByNameContainingIgnoreCase(name: String,page: Pageable): Page<Product>
    fun findByIsDeletedIsFalse():List<Product>
}