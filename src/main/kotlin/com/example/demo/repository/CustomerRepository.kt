package com.example.demo.repository

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import org.springframework.data.repository.CrudRepository

interface CustomerRepository :CrudRepository<Customer,Long>{
    fun findByName(name:String):Customer?
    fun findByNameEndingWithIgnoreCase(name:String):List<Customer>?

    fun findByNameContainingOrEmailContaining(name: String,email:String):List<Customer>?
    fun findByNameContainingAndIsDeletedIsFalseOrEmailContainingAndIsDeletedIsFalse(name: String,email:String):List<Customer>?

    fun findByDefaultAddress_ProvinceContaining(province:String):List<Customer>?
    fun findByUserStatus(status:UserStatus):List<Customer>?
}