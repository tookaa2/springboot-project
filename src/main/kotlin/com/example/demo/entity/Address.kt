package com.example.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Address (var homeAddress: String,
                    var subdistrict: String,
                    var district: String,
                    var province: String,
                    var poseCode: String,
                    @Id
                    @GeneratedValue
                    var id:Long? = null

)