package com.example.demo.entity.dto

import com.example.demo.entity.Product
data class SelectedProductDto(
        var quantity:Int?=null,
        var product: ProductDto?=null

)