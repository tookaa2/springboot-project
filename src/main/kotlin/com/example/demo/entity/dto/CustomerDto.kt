package com.example.demo.entity.dto

import com.example.demo.entity.Address
import com.example.demo.entity.UserStatus
import com.example.demo.security.entity.Authority

data class CustomerDto(
        var name:String?=null,
        var email:String?=null,
        var username: String?=null,
        var authorities: List<AuthorityDto> = mutableListOf(),
        var userStatus: UserStatus?= UserStatus.PENDING,
        var shippingAddress:List<Address>? = emptyList<Address>(),
        var billingAddress: Address?=null,
        var defaultAddress: Address?=null
)