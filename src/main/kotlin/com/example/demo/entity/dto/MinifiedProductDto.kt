package com.example.demo.entity.dto

data class MinifiedProductDto(
    var name:String?=null,
    var description:String?=null,
    var quantity:Int?=null
)