package com.example.demo.entity.dto

import com.example.demo.entity.Manufacturer

data class ProductDto(
        var name:String?=null,
        var description:String?=null,
        var price:Double?=null,
        var amountInStock:Int?=null,
        var imageUrl: String?=null,
        var manu: ManufacturerDto?=null,
        var id:Long? =null
)