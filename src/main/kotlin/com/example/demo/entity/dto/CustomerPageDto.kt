package com.example.demo.entity.dto


import com.example.demo.entity.ShoppingCart

data class CustomerPageDto(
        var totalPages: Int? = null,
        var totalElements: Long? = null,
        var customers: List<CustomerDto>? = mutableListOf()
)