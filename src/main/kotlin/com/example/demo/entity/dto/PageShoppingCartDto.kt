package com.example.demo.entity.dto

import com.example.demo.entity.ShoppingCart

data class PageShoppingCartDto(
    var totalPages:Int?=null,
    var totalElements:Long?=null,
    var shoppingcarts:List<ShoppingCartDto>? = mutableListOf()
)