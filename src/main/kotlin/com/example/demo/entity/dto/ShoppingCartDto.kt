package com.example.demo.entity.dto

import com.example.demo.entity.Customer
import com.example.demo.entity.SelectedProduct
import com.example.demo.entity.ShoppingCartStatus
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.OneToOne

data class ShoppingCartDto(
        var status: ShoppingCartStatus?=ShoppingCartStatus.WAIT,
        var customer: Customer?=null,
        var selectedProduct: List<SelectedProductDto>?= emptyList<SelectedProductDto>()
)