package com.example.demo.entity.dto

import com.example.demo.entity.Address
import com.example.demo.entity.dto.CustomerDto
import com.example.demo.entity.dto.SelectedProductDto

data class ShoppingResultDto(
        var customer:String?=null,
        var address:Address?=null,
        var products:List<MinifiedProductDto>?=null
)