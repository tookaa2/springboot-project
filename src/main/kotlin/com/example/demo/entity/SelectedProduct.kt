package com.example.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class SelectedProduct(var quantity:Int,
                           @OneToOne
                           var product: Product,
                           @Id
                           @GeneratedValue
                           var id:Long? = null

                           ){
}