package com.example.demo.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var status: ShoppingCartStatus,
                        @OneToOne
                        var customer:Customer){
    @Id
    @GeneratedValue
    var id:Long? =null
    @OneToMany
    var selectedProduct= mutableListOf<SelectedProduct>()
}