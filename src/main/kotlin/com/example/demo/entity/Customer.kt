package com.example.demo.entity

import com.example.demo.entity.Address
import com.sun.org.apache.xpath.internal.operations.Bool
import javax.persistence.*

@Entity
data class Customer(
    override var name:String?=null,
    override var email:String?=null,
    override var userStatus: UserStatus?= UserStatus.PENDING
) :User(name,email,userStatus){

    var isDeleted:Boolean?=false

    @ManyToMany
    var shippingAddress = mutableListOf<Address>()
    @ManyToOne
     var billingAddress: Address?=null
    @ManyToOne
     var defaultAddress: Address?=null
}