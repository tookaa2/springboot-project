package com.example.demo.entity

enum class ShoppingCartStatus{
    WAIT,CONFIRM,PAID,SENT,RECEIVED
}