package com.example.demo.dao

import com.example.demo.entity.Manufacturer
import com.example.demo.entity.dto.ManufacturerDto
import com.example.demo.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import util.MapperUtil
@Profile("db")
@Repository
class ManufacturerDaoDBImpl:ManufacturerDao{
    override fun findById(manuId: Long): Manufacturer? {
        return manufacturerRepository.findById(manuId).orElse(null)
    }

    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    override fun save(manu: Manufacturer): Manufacturer {
//        val maufacturer:Manufacturer= MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerRepository.save(manu)
    }

    override fun getManufacturers(): List<Manufacturer> {
       return manufacturerRepository.findAll().filterIsInstance(Manufacturer::class.java)
    }

}