package com.example.demo.dao

import com.example.demo.entity.Manufacturer
import com.example.demo.entity.dto.ManufacturerDto
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
@Profile("mem")
@Repository
class ManufacturerDaoImpl:ManufacturerDao{
    override fun findById(manuId: Long): Manufacturer? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun save(manu: Manufacturer): Manufacturer {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getManufacturers(): List<Manufacturer> {
        return mutableListOf(
                Manufacturer("Apple","053123456"),
                Manufacturer("Samsung","555666777888"),
                Manufacturer("CAMT","00000000")
                )
    }
}