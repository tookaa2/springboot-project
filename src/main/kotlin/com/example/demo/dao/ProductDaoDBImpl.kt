package com.example.demo.dao

import com.example.demo.entity.Product
import com.example.demo.entity.dto.ProductDto
import com.example.demo.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Repository
import util.MapperUtil

@Profile("db")
@Repository
class ProductDaoDBImpl: ProductDao{
    override fun findById(id: Long): Product? {
        return productRepository.findById(id).orElse(null)
    }

    override fun save(product: Product): Product {
        return productRepository.save(product)
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productRepository.findByNameContainingIgnoreCase(name,PageRequest.of(page,pageSize))
    }

    override fun getProductByPartialName(name: String): List<Product>? {
        return productRepository.findByNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var productRepository: ProductRepository

    override fun getProducts(): List<Product> {
//        return productRepository.findAll().filterIsInstance(Product::class.java)
        return productRepository.findByIsDeletedIsFalse().filterIsInstance(Product::class.java)
    }

    override fun getProductByName(name: String): Product? {
        return productRepository.findByName(name)
    }
}