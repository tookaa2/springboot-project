package com.example.demo.dao

import com.example.demo.entity.Customer
import com.example.demo.entity.Product
import com.example.demo.entity.UserStatus
import com.example.demo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl:CustomerDao{
    override fun findById(id: Long): Customer? {
        return cutomerRepository.findById(id).orElse(null)
    }

    override fun save(customer: Customer): Customer {
        return cutomerRepository.save(customer)
    }

    override fun getCustomerByStatus(status: UserStatus): List<Customer>? {
        return cutomerRepository.findByUserStatus(status)
    }

    override fun getCustomerByProvince(province: String): List<Customer>? {
        return cutomerRepository.findByDefaultAddress_ProvinceContaining(province)
    }

    override fun getCustomerByPartialEmail(email: String): List<Customer>? {
        return cutomerRepository.findByNameContainingAndIsDeletedIsFalseOrEmailContainingAndIsDeletedIsFalse(email,email)
//        return cutomerRepository.findByNameContainingOrEmailContaining(email,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer>? {
        return  cutomerRepository.findByNameEndingWithIgnoreCase(name)
    }

    @Autowired
    lateinit var cutomerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
        return cutomerRepository.findAll().filterIsInstance(Customer::class.java)
    }

    override fun getCustomerByName(name: String): Customer? {
        return cutomerRepository.findByName(name)
    }
}