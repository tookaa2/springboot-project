package com.example.demo.dao

import com.example.demo.entity.Address


interface AddressDao{
    fun saveAddress(addr:Address):Address
    fun findById(addrId: Long): Address


}