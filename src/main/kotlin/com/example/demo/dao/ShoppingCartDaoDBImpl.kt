package com.example.demo.dao

import com.example.demo.entity.Product
import com.example.demo.entity.SelectedProduct
import com.example.demo.entity.ShoppingCart
import com.example.demo.entity.ShoppingCartStatus
import com.example.demo.entity.dto.ShoppingCartDto
import com.example.demo.repository.CustomerRepository
import com.example.demo.repository.ProductRepository
import com.example.demo.repository.SelectedProductRepository
import com.example.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Profile("db")
@Repository
class ShoppingCartDaoDBImpl:ShoppingCartDao{
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository

    @Transactional
    override fun saveShoppingCartWithCustomerId(id: Long, shoppingCartDto: ShoppingCartDto): ShoppingCart {
        val customer = customerRepository.findById(id)
        var shoppingCart = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT,customer.orElse(null)))
        shoppingCartRepository.save(shoppingCart)
        for(selectProduct in shoppingCartDto.selectedProduct!!){

            val product:Product = productRepository.findById(selectProduct.product?.id!!).orElse(null)

            val selected=SelectedProduct(selectProduct.quantity!!,product)
            selectedProductRepository.save(selected)
            shoppingCart.selectedProduct.add(selected)
        }
        return shoppingCart

    }

    override fun getShoppingCartPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {

        return shoppingCartRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(name,PageRequest.of(page,pageSize))
    }

    override fun getShoppingCartPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findAll(PageRequest.of(page,pageSize))
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}