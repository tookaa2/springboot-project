package com.example.demo.dao

import com.example.demo.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao{
    fun getSelectedProductByName(name:String,page:Int,pageSize:Int):Page<SelectedProduct>
}