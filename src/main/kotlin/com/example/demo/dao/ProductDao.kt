package com.example.demo.dao

import com.example.demo.entity.Product
import com.example.demo.entity.dto.ProductDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface ProductDao {
    fun getProducts():List<Product>
    fun getProductByName(name:String):Product?
    fun getProductByPartialName(name: String): List<Product>?
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product>
    fun save(product: Product): Product
    fun findById(id: Long): Product?
}