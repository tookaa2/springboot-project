package com.example.demo.dao

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import com.example.demo.entity.dto.CustomerDto

interface CustomerDao{
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String):Customer?
    fun getCustomerByPartialName(name: String): List<Customer>?
    fun getCustomerByPartialEmail(email: String): List<Customer>?
    fun getCustomerByProvince(province: String): List<Customer>?
    fun getCustomerByStatus(status: UserStatus): List<Customer>?
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?



}