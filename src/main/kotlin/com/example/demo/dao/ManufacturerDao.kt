package com.example.demo.dao

import com.example.demo.entity.Manufacturer
import com.example.demo.entity.dto.ManufacturerDto
import org.springframework.stereotype.Repository


interface ManufacturerDao{
    fun getManufacturers():List<Manufacturer>
    fun save(manu: Manufacturer): Manufacturer
    fun findById(manuId: Long): Manufacturer?

}