package com.example.demo.dao

import com.example.demo.entity.ShoppingCart
import com.example.demo.entity.dto.ShoppingCartDto
import org.springframework.data.domain.Page

interface ShoppingCartDao{
    fun getShoppingCarts():List<ShoppingCart>
    fun getShoppingCartPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun saveShoppingCartWithCustomerId(id: Long, shoppingCartDto: ShoppingCartDto): ShoppingCart


}