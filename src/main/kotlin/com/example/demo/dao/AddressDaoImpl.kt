package com.example.demo.dao

import com.example.demo.entity.Address
import com.example.demo.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaoImpl:AddressDao{
    override fun findById(addrId: Long): Address {
        return addressRepository.findById(addrId).orElse(null)
    }


    @Autowired
    lateinit var addressRepository: AddressRepository
    override fun saveAddress(addr: Address): Address {
    return addressRepository.save(addr)
    }

}