package com.example.demo.service

import com.example.demo.entity.Address

interface AddressService{
    fun save(addr: Address): Address
}