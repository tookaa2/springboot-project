package com.example.demo.service

import com.example.demo.entity.ShoppingCart
import com.example.demo.entity.dto.ShoppingCartDto
import org.springframework.data.domain.Page

interface ShoppingCartService{
    fun getShoppingCarts():List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun save(id: Long, shoppingCartDto: ShoppingCartDto): ShoppingCart
}