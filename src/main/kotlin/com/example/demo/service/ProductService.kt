package com.example.demo.service

import com.example.demo.entity.Product
import com.example.demo.entity.dto.ProductDto
import org.springframework.data.domain.Page

interface ProductService{
    fun getProducts():List<Product>
    fun getProductByName(name:String):Product?
    abstract fun getProductByPartialName(name: String): List<Product>?
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product>
    fun save(product: Product): Product
    fun save(manuId:Long,product: Product): Product
    fun remove(id: Long): Product?
}