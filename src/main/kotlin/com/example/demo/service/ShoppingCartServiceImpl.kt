package com.example.demo.service

import com.example.demo.dao.ShoppingCartDao
import com.example.demo.entity.ShoppingCart
import com.example.demo.entity.dto.ShoppingCartDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl:ShoppingCartService{
    override fun save(id: Long, shoppingCartDto: ShoppingCartDto): ShoppingCart {
        return shoppingCartDao.saveShoppingCartWithCustomerId(id,shoppingCartDto)
    }

    override fun getShoppingCartPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartPartialNameWithPage(name, page, pageSize)
    }

    override fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartPage(page,pageSize)
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }
}