package com.example.demo.service

import com.example.demo.dao.ManufacturerDao
import com.example.demo.entity.Manufacturer
import com.example.demo.entity.dto.ManufacturerDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import util.MapperUtil

@Service
class ManufacturerServiceImpl:ManufacturerService{
    override fun save(manu: ManufacturerDto): Manufacturer {
        return manufacturerDao.save(MapperUtil.INSTANCE.mapManufacturer(manu))
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }
}