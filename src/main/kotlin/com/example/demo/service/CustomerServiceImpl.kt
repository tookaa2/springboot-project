package com.example.demo.service

import com.example.demo.dao.AddressDao
import com.example.demo.dao.CustomerDao
import com.example.demo.entity.Address
import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import com.example.demo.entity.dto.CustomerDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import util.MapperUtil
import javax.transaction.Transactional

@Service
class CustomerServiceImpl:CustomerService{
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer=customerDao.findById(id)
        customer?.isDeleted=true
        return customer
    }

    @Transactional
    override fun save(addrId: Long, customer: Customer): Customer {

//        customer.shippingAddress.add(customer.defaultAddress)
        val address=addressDao.findById(addrId)
        val addCustomer=customerDao.save(customer)
        addCustomer?.defaultAddress=address
        addCustomer?.shippingAddress.add(address)
        addCustomer?.billingAddress=address
        return addCustomer
    }

    @Autowired
    lateinit var addressDao: AddressDao
    override fun save(customer: Customer): Customer {
//        val defaultAddress:Address= customer.defaultAddress!!

            customer.shippingAddress.add(customer.defaultAddress!!)

        val address=customer.defaultAddress?.let{addressDao.saveAddress(it)}
        val addCustomer=customerDao.save(customer)
        addCustomer?.defaultAddress=address
        return addCustomer
    }


    override fun getCustomerByStatus(pending: UserStatus): List<Customer>? {
        return customerDao.getCustomerByStatus(pending)
    }

    override fun getCustomerByProvince(province: String): List<Customer>? {
        return customerDao.getCustomerByProvince(province)
    }

    override fun getCustomerByPartialName(name: String): List<Customer>? {
        return customerDao.getCustomerByPartialName(name)
    }

    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

    override fun getCustomerByName(name:String): Customer? {
        return customerDao.getCustomerByName(name)
    }

    override fun getCustomerByPartialEmail(email: String): List<Customer>? {
        return customerDao.getCustomerByPartialEmail(email)
    }
}