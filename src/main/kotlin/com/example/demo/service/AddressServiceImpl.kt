package com.example.demo.service

import com.example.demo.dao.AddressDao
import com.example.demo.entity.Address
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl:AddressService{
    @Autowired
    lateinit var addressDao: AddressDao
    override fun save(addr: Address): Address {
       return addressDao.saveAddress(addr)
    }

}