package com.example.demo.service

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import com.example.demo.entity.dto.CustomerDto

interface CustomerService{
    fun getCustomers():List<Customer>?
    fun getCustomerByName(name:String):Customer?
    fun getCustomerByPartialName(name: String): List<Customer>?
    fun getCustomerByPartialEmail(email: String): List<Customer>?
    fun getCustomerByProvince(province: String):  List<Customer>?
    fun getCustomerByStatus(pending: UserStatus): List<Customer>?
    fun save(customer: Customer): Customer
    fun save(addrId:Long,customer: Customer): Customer
    fun remove(id: Long): Customer?
}