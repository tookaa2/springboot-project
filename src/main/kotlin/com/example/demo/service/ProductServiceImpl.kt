package com.example.demo.service

import com.example.demo.dao.ManufacturerDao
import com.example.demo.dao.ProductDao
import com.example.demo.entity.Product
import com.example.demo.entity.dto.ProductDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import util.MapperUtil
import javax.transaction.Transactional

@Service
class ProductServiceImpl:ProductService{
    @Transactional
    override fun remove(id: Long): Product? {
        val product=productDao.findById(id)
        product?.isDeleted=true
        return product
    }

    @Transactional
    override fun save(manuId: Long, product: Product): Product {
        val manufaturer=manufacturerDao.findById(manuId)
        val product=productDao.save(product)
        product.manufacturer=manufaturer
        manufaturer?.products?.add(product)
        return product
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao

    @Transactional
    override fun save(product: Product): Product {
        val manufacturer=product.manufacturer?.let{manufacturerDao.save(it)}
        val product=productDao.save(product)
        manufacturer?.products?.add(product)
        return product
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productDao.getProductWithPage(name,page,pageSize)
    }

    override fun getProductByPartialName(name: String): List<Product>? {
        return productDao.getProductByPartialName(name)
    }

    @Autowired
    lateinit var productDao: ProductDao
    override fun getProducts(): List<Product> {
       return productDao.getProducts()
    }

    override fun getProductByName(name: String): Product? {
        return productDao.getProductByName(name)
    }
}