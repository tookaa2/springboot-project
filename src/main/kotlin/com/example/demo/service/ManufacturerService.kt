package com.example.demo.service

import com.example.demo.entity.Manufacturer
import com.example.demo.entity.dto.ManufacturerDto

interface ManufacturerService{
    fun getManufacturers():List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer


}