package com.example.demo.config

import com.example.demo.entity.*
import com.example.demo.repository.*
import com.example.demo.security.entity.Authority
import com.example.demo.security.entity.AuthorityName
import com.example.demo.security.entity.JwtUser
import com.example.demo.security.repository.AuthorityRepository
import com.example.demo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var dataLoader: DataLoader
    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name=AuthorityName.ROLE_ADMIN)
        val auth2=Authority(name=AuthorityName.ROLE_CUSTOMER)
        val auth3=Authority(name=AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val cus1 = Customer(name="สมชาติ",email="a@b.com")
        val custJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = cus1.email,
                enabled = true,
                firstname = cus1.name,
                lastname = "unknown"
        )
        customerRepository.save(cus1)
        userRepository.save(custJwt)
        cus1.jwtUser=custJwt
        custJwt.user=cus1
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)
    }
    @Transactional
    override fun run(args: ApplicationArguments?){

        var manu1 = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("Apple","0000000"))
        var manu3 = manufacturerRepository.save(Manufacturer("SAMSUNG","0000000"))
        var manu4 = manufacturerRepository.save(Manufacturer("Government","0000000"))
        var product1 = productRepository.save(Product("CAMT","The best college in CMU",0.0,5,"http"))
        manu1.products.add(product1)
        product1.manufacturer=manu1
        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย","แขวง ดินสอ","เขตดุสิต","กรุงเทพ","10123"))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่","ต.สุเทพ","อ.เมือง","จ.เชียงใหม่","50200"))
        var address3 = addressRepository.save(Address("ซักที่บนโลก","ต.สุขสันต์","อ.ในเมือง"," จ.ขอนแก่น","12457"))
        var customer1= customerRepository.save(Customer("Lung","pm@go.th",UserStatus.ACTIVE))
        customer1.shippingAddress= mutableListOf(address1)
        customer1.defaultAddress=address1
        customer1.billingAddress=address1

        var customer2= customerRepository.save(Customer("ชัชชาติ","chut@taopoon.com",UserStatus.ACTIVE))
        customer2.shippingAddress= mutableListOf(address2)
        customer2.defaultAddress=address2
        customer2.billingAddress=address2
        var customer3= customerRepository.save(Customer("ธนาธร","thanathorn@life.com",UserStatus.PENDING))
        customer3.shippingAddress= mutableListOf(address3)
        customer3.defaultAddress=address3
        customer3.billingAddress=address3

        var iPhoneProduct = productRepository.save(Product("iPhone","The best apple phone",0.0,5,"http"))
        var prayuthProduct = productRepository.save(Product("Prayuth","president",0.0,5,"http"))
        var iphonePurchase = selectedProductRepository.save(SelectedProduct(4,iPhoneProduct))
        var prayuthPurchase = selectedProductRepository.save(SelectedProduct(1,prayuthProduct))
        var shoppingCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT,customer1))
         shoppingCart1.selectedProduct= mutableListOf(iphonePurchase,prayuthPurchase)

        var Note9Product= productRepository.save(Product("Note 9","The best samsung phone",0.0,5,"http"))
        var shoppingCart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT,customer2))
        var prayuthPurchase2 = selectedProductRepository.save(SelectedProduct(1,prayuthProduct))
        var camtPurchase = selectedProductRepository.save(SelectedProduct(1,product1))
        var samsungPurchase= selectedProductRepository.save(SelectedProduct(2,Note9Product))
        shoppingCart2.selectedProduct= mutableListOf(prayuthPurchase2,camtPurchase,samsungPurchase)
        manu2.products.add(iPhoneProduct)
        iPhoneProduct.manufacturer=manu2
        manu3.products.add(Note9Product)
        Note9Product.manufacturer=manu3
        manu4.products.add(prayuthProduct)
        prayuthProduct.manufacturer=manu4
        dataLoader.loadData()
        loadUsernameAndPassword()
    }
}